#ifndef HEADER_H
#define HEADER_H

#include <iostream>
#include <vector>
#include <string>
#include <sstream> // for work with file in input.cpp
#include <iterator> // for work with istream_iterator in lexer.cpp
#include <utility> // for work with pair in parse.cpp
#include <cfenv>
#include <cstdint> // for int8_t int16_t int32_t ...

#include "classes/ExitException.hpp"
#include "classes/UnknownOp.hpp"
#include "classes/LexicalException.hpp"
#include "classes/IOperand.hpp"
#include "classes/VM.hpp"

#include "classes/Underflow.hpp"
#include "classes/Overflow.hpp"

// commands

#define PUSH   "push" // has arg
#define POP    "pop"
#define DUMP   "dump"
#define ASSERT "assert" // has arg
#define ADD    "add"
#define SUB    "sub"
#define MUL    "mul"
#define DIV    "div"
#define MOD    "mod"
#define PRINT  "print"
#define EXIT   "exit"

// input
const std::vector<std::string> input_from_file(const std::string src, std::vector<std::string> &errors);
const std::vector<std::string> input_from_stdin(std::vector<std::string> &errors) throw(ExitException);

// lexer
const std::vector<std::vector<std::string> >  lexer(std::vector<std::string> &src);


// parser
bool check_num(std::string &src, long long  &res);
bool check_num(std::string &src, long double &res);
std::string syntax(int n_line);
std::string uncomplite_command_error(int n_line);
std::string broken_arg(int n_line);
void parser(const std::vector<std::vector<std::string> > &src, std::vector<std::string> &errors, std::vector<std::pair<int, const IOperand*> > &to_do);

#endif