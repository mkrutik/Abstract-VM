#ifndef OVERFLOW_HPP
#define OVERFLOW_HPP

#include <exception>

class Overflow : public std::exception
{
    public:
        Overflow() throw();
        Overflow(const Overflow &src) throw();
        Overflow& operator = (const Overflow &src) throw();
        ~Overflow() throw();
        
        const char* what() const throw();
};

#endif