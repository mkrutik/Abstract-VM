#include "ExitException.hpp"

ExitException::ExitException() throw() {}

ExitException::~ExitException() throw() {}

const char* ExitException::what() const throw()
{
    return "\e[31mException: program doesn`t has exit instruction!\e[39m";
}

ExitException::ExitException(const ExitException &src) throw()
{
    *this = src;
}
ExitException& ExitException::operator = (const ExitException &src) throw()
{
    (void)src;
    return *this;
}