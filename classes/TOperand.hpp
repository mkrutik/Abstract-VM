#ifndef TOPERAND_HPP
#define TOPERAND_HPP
 
#include <string>
#include <exception>
#include <cstdint>
#include <limits>
#include <climits>
#include <cfloat>
#include <fenv.h>  // for floating point exception
#include "IOperand.hpp"
#include "OpFactory.hpp"

#include <iostream>

#include "Underflow.hpp"
#include "Overflow.hpp"

template<typename T>
class TOperand : public IOperand
{
private:
    T                       _val;
    std::string             _src;
    eOperandType            _type; // Int8 < Int16 < Int32 < Float < Double
    OPFactory               *_factory;

    template<typename V>
	void	check_num(eOperandType type, V val) const
	{
		if (type == Int8)
        {
            if (val > SCHAR_MAX)
                throw Overflow();
            else if (val < SCHAR_MIN)
                throw Underflow();
        }
		else if (type == Int16)
        {
            if (val > SHRT_MAX)
                throw Overflow();
            else if (val < SHRT_MIN)
                throw Underflow();
        }
		else if (type == Int32)
        {
            if (val > INT_MAX)
                throw Overflow();
            else if (val < INT_MIN)
                throw Underflow();
        }
        else if (type == Float)
        {
            if (val > FLT_MAX)
                throw Overflow();
            else if (val < FLT_MIN)
                throw Underflow();
        }
        else if (type == Double)
        {
            if (val > DBL_MAX)
                throw Overflow();
            else if (val < DBL_MIN)
                throw Underflow();
        }
	}

    TOperand(void);
    TOperand(const TOperand &src);
    TOperand& operator = (const TOperand &src);

public: 
    TOperand(eOperandType t, T val) : _val(val), _src((std::to_string(val))), _type(t), _factory()
    {
        _factory = new OPFactory();
    }
    ~TOperand(void) { delete _factory; }

    IOperand const* operator + (IOperand const & rhs) const
    {
        if (_type < rhs.getPrecision())
            return (rhs + *this);
        std::string tmp;
        if (_type < Float)
        {
            long long s1 = static_cast<long long>(_val);
            long long s2 = std::stoll(rhs.toString());
            long long res = s1 + s2;
            check_num(_type, res);
            tmp = std::to_string(static_cast<T>(res));
        }
        else
        {
            long double d1 = static_cast<long double>(_val);
            long double d2 = std::stold(rhs.toString());
            long double res = d1 + d2;
            check_num(_type, res);
            tmp = std::to_string(static_cast<T>(res));
        }

        return _factory->createOperand(_type, tmp);
    }

    IOperand const* operator - (IOperand const & rhs) const
    {
        if (_type < rhs.getPrecision())
        {    
            std::string tmp = rhs.toString();
            if (tmp[0] == '-')
                tmp[0] = '+';
            else
            {
                tmp = "-";
                tmp += rhs.toString();
            }
            const   IOperand *p = _factory->createOperand(rhs.getType(), tmp);
            const   IOperand *res = *p +*this;
            
            delete p;
            return res;
        }

        std::string tmp;
        if (_type < Float)
        {
            long long s1 = static_cast<long long>(_val);
            long long s2 = std::stoll(rhs.toString());
            long long res = s1 - s2;
            check_num(_type, res);
            tmp = std::to_string(static_cast<T>(res));
        }
        else
        {
            long double d1 = static_cast<long double>(_val);
            long double d2 = std::stold(rhs.toString());
            long double res = d1 - d2;
            check_num(_type, res);
            tmp = std::to_string(static_cast<T>(res));
        }
        return _factory->createOperand(_type, tmp);
    }

    IOperand const* operator * (IOperand const & rhs) const
    {
        if (_type < rhs.getPrecision())        
            return (rhs * (*this));

        std::string tmp;
        if (_type < Float)
        {
            long long s1 = static_cast<long long>(_val);
            long long s2 = std::stoll(rhs.toString());
            long long res = s1 * s2;
            check_num(_type, res);
            tmp = std::to_string(static_cast<T>(res));
        }
        else
        {
            long double d1 = static_cast<long double>(_val);
            long double d2 = std::stold(rhs.toString());
            long double res = d1 * d2;
            check_num(_type, res);
            tmp = std::to_string(static_cast<T>(res));
        }
        return _factory->createOperand(_type, tmp);
    }

    IOperand const* operator / (IOperand const & rhs) const
    {
        feclearexcept(FE_ALL_EXCEPT);
        eOperandType t = (_type < rhs.getPrecision() ? rhs.getType() : _type);

        std::string tmp;
        if (t < Float)
        {
            long long s1 = static_cast<long long>(_val);
            long long s2 = std::stoll(rhs.toString());
            if (s2 == 0)
                throw DivByZero();
            
            long long res = s1 / s2;
            check_num(_type, res);
            tmp = std::to_string(static_cast<double>(res));
        }
        else
        {
            long double d1 = static_cast<long double>(_val);
            long double d2 = std::stold(rhs.toString());
            if (d2 == 0)
                throw DivByZero();
            long double res = d1 / d2;

            if (fetestexcept(FE_UNDERFLOW))
                throw Overflow();
            else if (fetestexcept(FE_OVERFLOW))
                throw Underflow();
            else if (feclearexcept(FE_DIVBYZERO))
                throw DivByZero();
            feclearexcept(FE_ALL_EXCEPT);
            
            check_num(_type, res);
            tmp = std::to_string(static_cast<double>(res));
        }
        return _factory->createOperand(t, tmp);
    }

    IOperand const* operator % (IOperand const & rhs) const
    {
        if (_type == 3 || _type == 4 || rhs.getType() == 3 || rhs.getType() == 4)
            throw Unsupported();

        int n1 = static_cast<int>(_val);
        int n2  = std::stoi(rhs.toString());

        if (n2 == 0)
            throw ModByZero();
        
        eOperandType t = (_type < rhs.getPrecision() ? rhs.getType() : _type);
        std::string tmp(std::to_string(static_cast<T>(n1 % n2)));

        return _factory->createOperand(t, tmp);
    }

    std::string const& toString(void) const
    {
        const std::string *res = new std::string(std::to_string(_val));
        return *res; 
    }

    int getPrecision(void) const
    {
        return _type;
    }
    eOperandType getType(void) const
    {
        return _type;
    }



    class DivByZero : public std::exception
    {
        public:
            DivByZero(void) throw() {}
            DivByZero(const DivByZero &src) throw()
            {
                *this = src;
            }
            DivByZero& operator = (const DivByZero &src) throw()
            {
                (void)src;
                return *this;
            }
            ~DivByZero(void) throw() {}
            const char* what() const throw()
            {
                return "\e[31mException: operation division by zero!\e[39m";
            }
    };


    class ModByZero : public std::exception
    {
        public:
            ModByZero(void) throw() {}
            ModByZero(const ModByZero &src) throw()
            {
                *this = src;
            }
            ModByZero& operator = (const ModByZero &src) throw()
            {
                (void)src;
                return *this;
            }
            ~ModByZero(void) throw() {}
            const char* what() const throw()
            {
                return "\e[31mException: operation module by zero!\e[39m";
            }
    };

    class Unsupported : public std::exception
    {
        public:
            Unsupported(void) throw() {}
            Unsupported(const Unsupported &src) throw()
            {
                *this = src;
            }
            Unsupported& operator = (const Unsupported &src) throw()
            {
                (void)src;
                return *this;
            }
            ~Unsupported(void) throw() {}
            const char* what() const throw()
            {
                return "\e[31mOperation module unsupported with floating point value!\e[39m";
            }
    };
};

#endif