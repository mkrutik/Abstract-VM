#include "OpFactory.hpp"
#include "TOperand.hpp"

OPFactory::OPFactory(void) {}

OPFactory::~OPFactory(void) {}

template<typename V>
void	OPFactory::check_num(eOperandType type, V val) const
{
    if (type == Int8)
    {
        if (val > SCHAR_MAX)
            throw Overflow();
        else if (val < SCHAR_MIN)
            throw Underflow();
    }
    else if (type == Int16)
    {
        if (val > SHRT_MAX)
            throw Overflow();
        else if (val < SHRT_MIN)
            throw Underflow();
    }
    else if (type == Int32)
    {
        if (val > INT_MAX)
            throw Overflow();
        else if (val < INT_MIN)
            throw Underflow();
    }
    else if (type == Float)
    {
        if (val > FLT_MAX)
            throw Overflow();
        else if (val < -FLT_MAX)
            throw Underflow();
    }
    else if (type == Double)
    {
        if (val > DBL_MAX)
            throw Overflow();
        else if (val < -DBL_MAX)
            throw Underflow();
    }
}


IOperand const* OPFactory::createOperand(eOperandType type, std::string const & value ) const
{
    static func arr[5] = {&OPFactory::createInt8, &OPFactory::createInt16, &OPFactory::createInt32, &OPFactory::createFloat, &OPFactory::createDouble};
    if (type < Float)
        check_num(type, std::stoll(value));
    else
        check_num(type, std::stold(value));
    return (this->*arr[type])(value);
}

IOperand const* OPFactory::createInt8( std::string const & value ) const
{
    return new TOperand<int8_t>( Int8,std::stoi(value));
}

IOperand const* OPFactory::createInt16( std::string const & value ) const
{
    return new TOperand<int16_t>( Int16, std::stoi(value));
}

IOperand const* OPFactory::createInt32( std::string const & value ) const
{
    return new TOperand<int32_t>( Int32, std::stoi(value));
}

IOperand const* OPFactory::createFloat( std::string const & value ) const
{
    return new TOperand<float>( Float, std::stof(value));
}

IOperand const* OPFactory::createDouble( std::string const & value ) const
{
    return new TOperand<double>( Double, std::stod(value));
}
