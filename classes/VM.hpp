#ifndef VM_HPP
#define VM_HPP

#include <iostream>
#include "IOperand.hpp"
#include "../header.h"
#include <exception>
#include <iomanip>
#include "SteckOp.hpp"

class VM
{
private:
    SteckOp     _stack;

    VM(const VM &src);
    VM& operator = (const VM &src);

    void    dump(void);

public:
    VM(void);
    ~VM(void);

    void    execute(std::vector<std::pair<int, const IOperand*> > &src);

    class EmptyPOP : public std::exception
    {
        public:
            EmptyPOP() throw();
            EmptyPOP(const EmptyPOP &src) throw();
            EmptyPOP& operator = (const EmptyPOP &src) throw();
            ~EmptyPOP() throw();

            const char* what() const throw();
    };

    class AssertEx : public std::exception
    {
        public:
            AssertEx() throw();
            AssertEx(const AssertEx &src) throw();
            AssertEx& operator = (const AssertEx &src) throw();
            ~AssertEx() throw();

            const char* what() const throw();
    };

    class LessArg : public std::exception 
    {
        public:
            LessArg() throw();
            LessArg(const LessArg &src) throw();
            LessArg& operator = (const LessArg &src) throw();
            ~LessArg() throw();

            const char* what() const throw();
    };
    class NotSupport : public std::exception 
    {
        public:
            NotSupport() throw();
            NotSupport(const NotSupport &src) throw();
            NotSupport& operator = (const NotSupport &src) throw();
            ~NotSupport() throw();

            const char* what() const throw();
    };
};

#endif