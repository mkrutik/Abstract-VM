#include "SteckOp.hpp"

SteckOp::SteckOp() : _data() {}

SteckOp::SteckOp(SteckOp &src) : _data()
{
    SteckOp::iterator start = src.begin();
    SteckOp::iterator end = src.end();
    
    for (; start != end; ++start)
    {
        _data.push_back(&(*start));
    }
}

SteckOp&    SteckOp::operator = (SteckOp &src)
{
    _data.clear();

    SteckOp::iterator start = src.begin();
    SteckOp::iterator end = src.end();
    for (; start != end; ++start)
    {
        _data.push_back(&(*start));
    }
    return *this;
}

SteckOp::~SteckOp()
{
    if (_data.size() != 0)
    {
        for (unsigned long i = 0; i < _data.size(); i++)
        {
            delete _data[i];
        }
        _data.clear();
    }
}

const IOperand&   SteckOp::top() const
{
    return *_data.back();
}

void                    SteckOp::pop()
{
    _data[(_data.size() - 1)]->~IOperand();
    _data.pop_back();
}

bool                    SteckOp::empty() const
{
    return _data.empty();
}

size_t                  SteckOp::size() const
{
    return _data.size();
}


void                    SteckOp::push(const IOperand &value)
{
    _data.push_back(&value);
}



SteckOp::iterator SteckOp::begin()
{
    return iterator( _data.begin());
}

SteckOp::iterator SteckOp::end()
{
    return iterator( _data.end());
}




SteckOp::iterator SteckOp::iterator::operator ++ ()
{
    iterator i = *this;
    _it++;
    return i;
}
        

SteckOp::iterator SteckOp::iterator::operator ++ (int junk)
{
    junk++;
    _it++;
    return *this;
}

SteckOp::iterator SteckOp::iterator::operator -- ()
{
    iterator i = *this;
    _it--;
    return i;
}

SteckOp::iterator SteckOp::iterator::operator -- (int junk)
{
    junk--;
    _it--;
    return *this;
}

const IOperand& SteckOp::iterator::operator * ()
{
    return **_it;
}

const IOperand* SteckOp::iterator::operator -> ()
{
    return *_it;
}

bool SteckOp::iterator::operator == (const iterator& rhs)
{
    return _it == rhs._it;
}

bool SteckOp::iterator::operator != (const iterator& rhs)
{
    return _it != rhs._it; 
}

SteckOp::iterator::~iterator() {}

SteckOp::iterator&    SteckOp::iterator::operator = (const iterator &src)
{
    _it = src._it;
    return *this;
}

SteckOp::iterator::iterator(const iterator &src) : _it(src._it) {}