#include "Overflow.hpp"

Overflow::Overflow() throw() {}
Overflow::Overflow(const Overflow &src) throw()
{
    *this = src;
}
Overflow& Overflow::operator = (const Overflow &src) throw()
{
    (void)src;
    return *this;
}
Overflow::~Overflow() throw() {}
const char* Overflow::what() const throw()
{
    return "\e[31mException: Overflow current value!\e[39m";
}