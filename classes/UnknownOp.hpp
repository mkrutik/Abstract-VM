#ifndef UNKNOWNOP_HPP
#define UNKNOWNOP_HPP

#include <exception>

class UnknownOP : public std::exception
{
public:
    UnknownOP() throw();
    UnknownOP(const UnknownOP &src) throw();
    UnknownOP& operator = (const UnknownOP &src) throw();
    ~UnknownOP() throw();

    const char* what() const throw();
};

#endif