#include "Underflow.hpp"

Underflow::Underflow() throw() {}
Underflow::Underflow(const Underflow &src) throw()
{
    *this = src;
}
Underflow& Underflow::operator = (const Underflow &src) throw()
{
    (void)src;
    return *this;
}
Underflow::~Underflow() throw() {}
const char* Underflow::what() const throw()
{
    return "\e[31mException: Underflow current value!\e[39m";
}