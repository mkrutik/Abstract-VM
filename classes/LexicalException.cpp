#include "LexicalException.hpp"

LexicalException::LexicalException() throw() {}

LexicalException::LexicalException(const LexicalException &src) throw()
{
    *this = src;
}

LexicalException& LexicalException::operator = (const LexicalException &src) throw()
{
    (void)src;
    return *this;
}

LexicalException::~LexicalException() throw() {}

const char* LexicalException::what() const throw()
{
    return "\e[31mException: program has at least one lexical error!\e[39m";
}