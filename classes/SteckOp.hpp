#ifndef STECKOP_HPP
#define STECKOP_HPP

#include <stack>
#include <iterator>
#include <vector>
#include <cstddef>

#include "IOperand.hpp"

class SteckOp
{
private:
    std::vector<const IOperand*> _data;

    SteckOp(SteckOp &src);
    SteckOp& operator = (SteckOp &src);

public:
    SteckOp();
    ~SteckOp();

    void                push(const IOperand &value);
    
    const IOperand&     top() const;
    void                pop();
    
    bool                empty() const;
    size_t              size() const;
    
    class iterator
    {
        protected:
            std::vector<const IOperand*>::iterator  _it;

        public:
            ~iterator();
            iterator&   operator = (const iterator &src);
            iterator(const iterator &src);
            iterator(std::vector<const IOperand*>::iterator src)
            : _it(src)
            {}


            iterator    operator ++ ();
            iterator    operator ++ (int junk);

            iterator    operator -- ();
            iterator    operator -- (int junk);


            const IOperand&     operator * ();
            const IOperand*    operator -> ();
            bool        operator == (const iterator& rhs);
            bool        operator != (const iterator& rhs);

    };

    iterator    begin();
    iterator    end();
};

#endif