#include "VM.hpp"

VM::VM(void) : _stack() {};

VM::~VM(void)
{
    for (unsigned long i = 0; i < _stack.size(); i++)
    {
        _stack.pop();
    }
}

void    VM::dump(void)
{
    if (_stack.size() != 0)
    {
        SteckOp::iterator start =   _stack.begin();
        SteckOp::iterator end =     _stack.end();
        --end;
        --start;
        while (end != start)
        {
            if ((*end).getType() == 3 || (*end).getType() == 4)
            {
                double res = std::stod((*end).toString());
                std::cout << std::fixed << std::setprecision(2) << res << std::endl;
            }
            else
                std::cout << std::fixed << std::setprecision(2) << (*end).toString() << std::endl;
            --end;
        }
    }
}

void    VM::execute(std::vector<std::pair<int, const IOperand*> > &src)
{
    for (unsigned long i = 0; i < src.size(); i++)
    {
        const IOperand *n1;
        const IOperand *n2;
        const IOperand *res;

        if (src[i].first == 0) //push
        {
            _stack.push(*src[i].second);
            src[i].second = NULL;
        }
        else if (src[i].first == 1) // pop
        {
            if (_stack.size() == 0)
                throw EmptyPOP();
            _stack.pop();
        }
        else if (src[i].first == 2) //dump
            this->dump();
        else if (src[i].first == 3) //assert
        {
            if (_stack.size() < 1)
                throw LessArg();
            n1 = &_stack.top();
            if (n1->getType() != src[i].second->getType() || n1->toString() != src[i].second->toString())
            {
                delete src[i].second;
                src[i].second = NULL;
                throw AssertEx();
            }    
        }
        else if (src[i].first == 4) // add
        {
            if (_stack.size() < 2)
                throw LessArg();
            n1 = &_stack.top();
            _stack.pop();
            n2 = &_stack.top();
            _stack.pop();
            res = *n1 + *n2;
            _stack.push(*res);
        }
        else if (src[i].first == 5) //sub
        {
            if (_stack.size() < 2)
                throw LessArg();
            n1 = &_stack.top();
            _stack.pop();
            n2 = &_stack.top();
            _stack.pop();
            res = *n2 - *n1;
            _stack.push(*res);
        }
        else if (src[i].first == 6) // mul
        {
            if (_stack.size() < 2)
                throw LessArg();
            n1 = &_stack.top();
            _stack.pop();
            n2 = &_stack.top();
            _stack.pop();
            res = *n1 * *n2;
            _stack.push(*res);
        }
        else if (src[i].first == 7) //div
        {
            if (_stack.size() < 2)
                throw LessArg();
            n1 = &_stack.top();
            _stack.pop();
            n2 = &_stack.top();
            _stack.pop();
            res = *n2 / *n1;
            _stack.push(*res);
        }
        else if (src[i].first == 8) //mod
        {
            if (_stack.size() < 2)
                throw LessArg();
            n1 = &_stack.top();
            _stack.pop();
            n2 = &_stack.top();
            _stack.pop();
            if (n1->getPrecision() > 2 || n2->getPrecision() > 2)
                throw NotSupport();

            res = *n2 % *n1;
            _stack.push(*res);
        }
        else if (src[i].first == 9) //print
        {
            if (_stack.size() < 1)
                throw LessArg();
            n1 = &_stack.top();
            if (n1->getType() != Int8)
                throw AssertEx();
            else
            {
                int x = std::stoi(n1->toString());
                std::cout << "[" << (static_cast<char>(x)) << "]" << std::endl;
            }
        }
    }
}


VM::EmptyPOP::EmptyPOP() throw() {}

VM::EmptyPOP::EmptyPOP(const EmptyPOP &src) throw()
{
    *this = src;
}
VM::EmptyPOP& VM::EmptyPOP::operator = (const EmptyPOP &src) throw()
{
    (void)src;
    return *this;
}
VM::EmptyPOP::~EmptyPOP() throw() {}

const char* VM::EmptyPOP::what() const throw()
{
    return "\e[31mException: Invalid operation pop on empty stack!\e[39m";
}


VM::AssertEx::AssertEx() throw(){}
VM::AssertEx::AssertEx(const AssertEx &src) throw()
{
    *this = src;
}
VM::AssertEx& VM::AssertEx::operator = (const AssertEx &src) throw()
{
    (void)src;
    return  *this;
}
VM::AssertEx::~AssertEx() throw() {}

const char*  VM::AssertEx::what() const throw()
{
    return "\e[31mException: Operation Assert return FALSE, or Operation print can't run!\e[39m";
}

VM::LessArg::LessArg() throw() {}
VM::LessArg::LessArg(const LessArg &src) throw()
{
    *this = src;
}
VM::LessArg& VM::LessArg::operator = (const LessArg &src) throw()
{
    (void)src;
    return *this;
}
VM::LessArg::~LessArg() throw() {}

const char* VM::LessArg::what() const throw()
{
    return "\e[31mException: Stack has less than two values!\e[39m";
}


VM::NotSupport::NotSupport() throw() {}
VM::NotSupport::NotSupport(const NotSupport &src) throw()
{
    *this = src;
}

VM::NotSupport& VM::NotSupport::operator = (const NotSupport &src) throw()
{
    (void)src;
    return *this;
}

VM::NotSupport::~NotSupport() throw(){}

const char* VM::NotSupport::what() const throw()
{
    return "\e[31mException: Operation module not suported for float and double values!\e[39m";
}
