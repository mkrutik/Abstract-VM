#include "UnknownOp.hpp"

UnknownOP::UnknownOP() throw() {}

UnknownOP::~UnknownOP() throw() {}

const char* UnknownOP::what() const throw()
{
    return "\e[31mException: Unknown instruction!\e[39m";
}

UnknownOP::UnknownOP(const UnknownOP &src) throw()
{
    *this = src;
}

UnknownOP& UnknownOP::operator = (const UnknownOP &src) throw()
{
    (void)src;
    return *this;
}