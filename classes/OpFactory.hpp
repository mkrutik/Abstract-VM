#ifndef OPFACTORY_HPP
#define OPFACTORY_HPP

#include <cstdint>
#include <string>

#include "IOperand.hpp"

class OPFactory
{
private:
    typedef IOperand const*  (OPFactory::*func)(std::string const &) const;

    template<typename V>
    void	check_num(eOperandType type, V val) const;

    IOperand const* createInt8( std::string const & value ) const;
    IOperand const* createInt16( std::string const & value ) const;
    IOperand const* createInt32( std::string const & value ) const;
    IOperand const* createFloat( std::string const & value ) const;
    IOperand const* createDouble( std::string const & value ) const;

    OPFactory(const OPFactory &src);
    OPFactory& operator = (const OPFactory &src);

public:

    OPFactory(void);
    ~OPFactory(void);

    IOperand const * createOperand(eOperandType type, std::string const & value ) const;
};

#endif