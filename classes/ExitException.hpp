#ifndef EXITEXCEPTION_HPP
#define EXITEXCEPTION_HPP

#include <exception>

class ExitException : public std::exception
{

public:
    ExitException() throw();
    ExitException(const ExitException &src) throw();
    ExitException& operator = (const ExitException &src) throw();
    ~ExitException() throw();

    const char* what() const throw();
};

#endif