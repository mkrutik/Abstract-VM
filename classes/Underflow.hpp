#ifndef UNDERFLOW_HPP
#define UNDERFLOW_HPP

#include <exception>

class Underflow  : public std::exception
{
    public:
        Underflow() throw();
        Underflow(const Underflow &src) throw();
        Underflow& operator = (const Underflow &src) throw();
        ~Underflow() throw();
        const char* what() const throw();
};

#endif