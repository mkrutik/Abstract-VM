#ifndef LEXICALEXCEPTION_HPP
#define LEXICALEXCEPTION_HPP

#include <exception>

class LexicalException : public std::exception
{
public:
    LexicalException() throw();
    LexicalException(const LexicalException &src) throw();
    LexicalException& operator = (const LexicalException &src) throw();
    ~LexicalException() throw();

    const char* what() const throw();
};

#endif