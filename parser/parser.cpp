#include "../header.h"
#include "../classes/IOperand.hpp"
#include "../classes/OpFactory.hpp"
#include <type_traits>


static int  parse_op_name(const std::string &src)
{
    if (!src.compare(PUSH))
        return (0);
    else if (!src.compare(POP))
        return (1);
    else if (!src.compare(DUMP))
        return (2);
    else if (!src.compare(ASSERT))
        return (3);
    else if (!src.compare(ADD))
        return (4);
    else if (!src.compare(SUB))
        return (5);
    else if (!src.compare(MUL))
        return (6);
    else if (!src.compare(DIV))
        return (7);
    else if (!src.compare(MOD))
        return (8);
    else if (!src.compare(PRINT))
        return (9);
    else if (!src.compare(EXIT))
        return (10);

    return (-1); // unknown op name
}

void parser(const std::vector<std::vector<std::string> > &src, std::vector<std::string> &errors, std::vector<std::pair<int, const IOperand*> > &to_do)
{
    OPFactory *factory = new OPFactory();

    bool  exit_command = false;
    int   op_code;

    for (unsigned long i = 0; !exit_command && i < src.size(); i++)
    {
        std::pair<int, const IOperand*> p;
        
        p.first = -2;
        p.second = NULL;

        for (unsigned long j = 0; j < src[i].size(); j++)
        {
            if (j == 0) // there must be command name
            {
                if (src[i][j] == ";" || src[i][j][0] == ';') // comment
                    break ;
                else
                {
                    op_code = parse_op_name(src[i][j]);
                    if (op_code == -1) // unknown command
                        throw UnknownOP();
                    if (op_code == 10)
                        exit_command = true;
                    p.first = op_code;
                }
            }
            else if (src[i][j] == ";" || src[i][j][0] == ';') // comment
                break ;
            else
            {
                if (p.second != NULL)
                {
                    errors.push_back(syntax(i));
                    break;
                }
                std::string::size_type  s;
                std::string::size_type  e;
                bool                    stat;
                
                if (src[i][j].find("int8", 0) != std::string::npos)
                {
                    s = src[i][j].find("(", 4);
                    e = src[i][j].find(")");
                    long long res = 0;
                    if (s != std::string::npos && e != std::string::npos && e != (s + 1))
                    {
                        std::string tmp( (src[i][j].begin() + s + 1), (src[i][j].begin() + e));
                        stat = check_num(tmp, res);
                        if (!stat)
                            errors.push_back(syntax(i));
                        else
                            p.second = factory->createOperand(Int8, tmp);
                    }
                    else
                        errors.push_back(broken_arg(i));
                }
                else if (src[i][j].find("int16", 0) != std::string::npos)
                {
                    s = src[i][j].find("(", 5);
                    e = src[i][j].find(")");
                    long long res = 0;
                    if (s != std::string::npos && e != std::string::npos && e != (s + 1))
                    {
                        std::string tmp( (src[i][j].begin() + s + 1), (src[i][j].begin() + e));
                        stat = check_num(tmp, res);
                        if (!stat)
                            errors.push_back(syntax(i));
                        else
                            p.second = factory->createOperand(Int16, tmp);
                    }
                    else
                        errors.push_back(broken_arg(i));
                }
                else if (src[i][j].find("int32", 0) != std::string::npos)
                {
                    s = src[i][j].find("(", 5);
                    e = src[i][j].find(")");
                    long long res = 0;
                    if (s != std::string::npos && e != std::string::npos && e != (s + 1))
                    {
                        std::string tmp( (src[i][j].begin() + s + 1), (src[i][j].begin() + e));
                        stat = check_num(tmp, res);
                        if (!stat)
                            errors.push_back(syntax(i));
                        else
                            p.second = factory->createOperand(Int32, tmp);
                    }
                    else
                        errors.push_back(broken_arg(i));
                }
                else if (src[i][j].find("float", 0) != std::string::npos)
                {
                    s = src[i][j].find("(", 5);
                    e = src[i][j].find(")");
                    long double res = 0;
                    if (s != std::string::npos && e != std::string::npos && e != (s + 1))
                    {
                        std::string tmp( (src[i][j].begin() + s + 1), (src[i][j].begin() + e));
                        stat = check_num(tmp, res);
                        if (!stat)
                            errors.push_back(syntax(i));
                        else
                            p.second = factory->createOperand(Float, tmp);
                    }
                    else
                        errors.push_back(broken_arg(i));
                }
                else if (src[i][j].find("double", 0) != std::string::npos)
                {
                    s = src[i][j].find("(", 6);
                    e = src[i][j].find(")");
                    long double res = 0;
                    if (s != std::string::npos && e != std::string::npos && e != (s + 1))
                    {
                        std::string tmp( (src[i][j].begin() + s + 1), (src[i][j].begin() + e));
                        stat = check_num(tmp, res);
                        if (!stat)
                            errors.push_back(syntax(i));
                        else
                            p.second = factory->createOperand(Double, tmp);
                    }
                    else
                        errors.push_back(broken_arg(i));
                }
                else
                    errors.push_back(syntax(i)); // if it`s not argument that must be some syntax
            }
        }
        if ((p.first == 0 || p.first == 3) && p.second == NULL) // missing argument
            errors.push_back(uncomplite_command_error(i));
        if (exit_command == true)
            break;
        if (p.first >= 0)
            to_do.push_back(p);
    }
    if (exit_command == false)
        throw ExitException();
    
    delete factory;
}

