#include "../header.h"

std::string syntax(int n_line)
{
    std::string tmp = "Line ";
    tmp += std::to_string((n_line + 1));
    tmp += " syntax error!";
    return tmp;
}

std::string uncomplite_command_error(int n_line)
{
    std::string tmp = "Line ";
    tmp += std::to_string((n_line + 1));
    tmp += " missing argument for this command!";
    return tmp;
}

std::string broken_arg(int n_line)
{
    std::string tmp = "Line ";
    tmp += std::to_string((n_line + 1));
    tmp += " has broken argument!";
    return tmp; 
}