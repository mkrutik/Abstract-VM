#include "../header.h"

#include "../classes/Underflow.hpp"
#include "../classes/Overflow.hpp"
#include <regex>

bool check_num(std::string &src, long long &res) // int
{
    std::regex r("[-]?[0-9]+");
    
    if ( !std::regex_match(src.c_str(), r) )
    {
        std::cout << "Int argument hasn`t been tested for compliance with regular expression" << std::endl;
        throw LexicalException();
    }
    int counter = 0;
    int minus = 0;
    for (unsigned long i = 0; i < src.size(); i++)
    {
        if (src[i] == '-')
            minus++;
        if (src[i] >= '0' && src[i] <= '9')
            counter++;
    }
    if (counter > 19 && minus == 0)
        throw Overflow();
    else if (counter > 19 && minus == 1)
        throw Underflow();

    try
    {
        res = std::stoll(src);
    }
    catch(...)
    {
        std::cout << "Int argument hasn`t been tested for compliance with regular expression" << std::endl;
        throw LexicalException();
    }
    std::string tmp = std::to_string(res);

    if (!src.compare(tmp))
        return true;
    
    return true;
}

bool check_num(std::string &src, long double &res) // float
{
    std::regex r("[-]?[0-9]+[.][0-9]+");
    
    if ( !std::regex_match(src.c_str(), r))
    {
        std::cout << "Floating point argument hasn`t been tested for compliance with regular expression" << std::endl;
        throw LexicalException();
    }
    try
    {
        res = std::stold(src);
    }
    catch(...)
    {
        std::cout << "Floating point argument hasn`t been tested for compliance with regular expression" << std::endl;
        throw LexicalException();
    }
    std::string tmp = std::to_string(res);

    bool can_break = false;

    unsigned long i = 0;
    for ( ; i < src.size() && i < tmp.size(); i++)
    {
        if (src[i] != tmp[i])
        {
            if (can_break)
                break;
            else
                return false;
        }
        if (src[i] == '.')
            can_break = true;
    }
    for ( ; i < src.size(); i++)
    {
        if (src[i] < '0' || src[i] > '9')
            return false;
    }
    
    return true;
}