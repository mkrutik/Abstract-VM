#include "../header.h"

#include <fstream>

const std::vector<std::string> input_from_file(const std::string src, std::vector<std::string> &errors)
{
    std::vector<std::string> res;

    std::ifstream file;
    file.open(src.c_str(), std::ios::in);

    if (file.is_open())
    {
        std::string tmp;
        while (std::getline(file,tmp))
        {
            res.push_back(tmp);
            tmp.clear();
        }
        file.close();
        if (res.size() == 0)
            errors.push_back("File is empty!");
    }
    else
    {
        std::string t = "File \"";
        t += src.c_str();
        t += "\" not fonund!";
        errors.push_back(t);
        // ERROR file not found
    }

    return res;
}

const std::vector<std::string> input_from_stdin(std::vector<std::string> &errors) throw(ExitException)
{
    std::vector<std::string> res;

    std::string tmp;
    bool exit_command = false;

    while (getline(std::cin, tmp))
    {
        if (tmp == ";;")
        {
            exit_command = true;
            break ;
        }
        res.push_back(tmp);
        tmp.clear();
    }
    if (exit_command == false)
        throw ExitException();
    if (res.size() == 0)
        errors.push_back("Nothing has been entered!");
    return res;
}