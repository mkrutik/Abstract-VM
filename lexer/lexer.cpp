#include "../header.h"

static std::string& do_better(std::string &src)
{
    std::string::size_type n;
    while ((n = src.find(" (")) != std::string::npos)
        src.replace(n, 2, "(");
    if ((n = src.find(")")) != std::string::npos)
        src.replace(n, 1, ") ");


    while ((n = src.find("( ")) != std::string::npos)
        src.replace(n, 2, "(");
    while ((n = src.find(" )")) != std::string::npos)
        src.replace(n, 2, ")");


    if ((n = src.find("push")) != std::string::npos)
        src.replace(n, 4, "push ");
    else if ((n = src.find("pop")) != std::string::npos)
        src.replace(n, 3, "pop ");
    else if ((n = src.find("dump")) != std::string::npos)
        src.replace(n, 4, "dump ");
    else if ((n = src.find("assert")) != std::string::npos)
        src.replace(n, 6, "assert ");
    else if ((n = src.find("add")) != std::string::npos)
        src.replace(n, 3, "add ");
    else if ((n = src.find("sub")) != std::string::npos)
        src.replace(n, 3, "sub ");
    else if ((n = src.find("mul")) != std::string::npos)
        src.replace(n, 3, "mul ");
    else if ((n = src.find("div")) != std::string::npos)
        src.replace(n, 3, "div ");
    else if ((n = src.find("mod")) != std::string::npos)
        src.replace(n, 3, "mod ");
    else if ((n = src.find("print")) != std::string::npos)
        src.replace(n, 5, "print ");
    else if ((n = src.find("exit")) != std::string::npos)
        src.replace(n, 4, "exit ");

    return src;
}

std::vector<std::string> split_string(std::string &src)
{
    // splitting string by whitespace

    do_better(src);

    std::istringstream buffer(src);
    std::vector<std::string> res( (std::istream_iterator<std::string>(buffer)), (std::istream_iterator<std::string>()) );

    return res;
}


const std::vector<std::vector<std::string> >  lexer(std::vector<std::string> &src)
{
    std::vector<std::vector<std::string> > res;

    for (unsigned long i = 0; i < src.size(); i++)
    {
        // splitting all line to tokens
        res.push_back(split_string(src[i]));
    }

    return res;
}