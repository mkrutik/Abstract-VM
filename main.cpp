#include "header.h"

void    error_print(const std::vector<std::string> &src)
{
    std::cout << "\e[31m";
    for (unsigned long i = 0; i < src.size(); i++)
    {
        std::cout << "ERROR: " << src[i] << std::endl;
    }
    std::cout << "\e[39m";
}

void    prog_body(std::vector<std::string> &input, std::vector<std::string> &errors)
{
    std::vector<std::pair<int, const IOperand*> > to_do;
    std::vector<std::vector<std::string> > res;

    res = lexer(input);
    try 
    {
        parser(res, errors, to_do);
        if (errors.size() != 0)
        {
            error_print(errors);
            throw LexicalException();
        }
        else
        {
            VM machine;
            machine.execute(to_do);
        }
    }
    catch(std::exception &e)
    {
        std::cout << e.what() << std::endl;
    }
    for (unsigned long i = 0; i < to_do.size(); i++)
    {
        if (to_do[i].second != NULL)
            delete to_do[i].second;
    }
    res.clear();    
    to_do.clear();    
}

int main(int argc, char **argv)
{    
    std::vector<std::string> input;
    std::vector<std::string> errors;


    if (argc >= 2)
    {
        int i = 1;
        while (i < argc)
        {
            try
            {
                input =  input_from_file(argv[i], errors);
                if (errors.size() != 0)
                    error_print(errors);
                prog_body(input, errors);                            
            }
            catch(std::exception &e)
            {
                std::cout << e.what() << std::endl;
            }

            if (argc != 2)
                std::cout << "\e[92m----* Program " << argv[i] << " *----\e[39m" << std::endl;
            errors.clear();
            input.clear();
            i++;
        }
    }
    else
    {
        try
        {
            input = input_from_stdin(errors); //throw exception
            if (errors.size() != 0)
                error_print(errors);
            prog_body(input, errors);        
        }
        catch(std::exception &e)
        {
            std::cout << e.what() << std::endl;
        }
        
        errors.clear();
        input.clear();
    }

    return (0);
}