NAME = avm

OBJ = main.o \
	input/input.o \
	lexer/lexer.o \
	parser/parser.o  parser/check_num.o parser/parse_error.o \
	classes/ExitException.o \
	classes/UnknownOp.o \
	classes/LexicalException.o \
	classes/OpFactory.o \
	classes/SteckOp.o \
	classes/VM.o \
	classes/Overflow.o \
	classes/Underflow.o

FLAGS = -Wall -Wextra -Werror -std=c++11

all: $(OBJ)
	clang++ $(FLAGS) $(OBJ) -o $(NAME)


%.o : %.cpp

	clang++ $(FLAGS) -o $@ -c $<


clean:
	rm -f $(OBJ)

fclean: clean
	rm -f $(NAME)

re: fclean all